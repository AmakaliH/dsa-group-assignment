import ballerina/grpc;

listener grpc:Listener ep = new (9090);

//#################################################
//      FUNCTION MAP DEFINITION
//#################################################

map<Function> functions = {};

//################################################

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "functionRepository" on ep {

    //#################################################
    //      ADD NEW FN
    //#################################################
    remote function add_new_fn(AddNewFnRequest value) returns AddNewFnResponse|error {
        Function fn = value.fn;

        //#################################################
        //      CHECK IF FUNCTION ALREADY EXISTS
        //#################################################
        if functions.hasKey(fn.details.name) {

            //######################################################
            //      CHECK IF CORRECT NUM OF VERSIONS SPECIFIED
            //      EXPECTED NUM IS 1
            //######################################################
            if fn.versions.length() > 1 {
                return error("Only 1 new version can be added at a time");
            }
            if fn.versions.length() == 0 {
                return error("No new version specified");
            }
            //######################################################

            Function currentFn = functions.get(fn.details.name);

            //######################################################
            //      IF THE CURRENT FUNCTION'S VERSIONS ARR IS EMPTY
            //      PUSH NEW FN VERSION AND RETURN
            //######################################################
            if currentFn.versions.length() == 0 {
                currentFn.versions.push(fn.versions[0]);
                return {message: "Successfully added new version"};
            }

            //####################################################
            //      CHECK IF VERSION ALREADY EXISTS
            //      VERSIONS CONTINUE BASED OFF OF THE 
            //      LAST VERSION STORED. eg [1,2,3] 3 WOULD
            //      BE THE LAST VERSION AND NEXT VERSION 
            //      EXPECTED IS 4
            //      IF NEW VERSION IS NOT LATEST VERSION + 1 
            //      RETURN ERROR, ELSE PUSH NEW VERSION TO ARR
            //####################################################
            if fn.versions[0].versionNumber <= currentFn.versions[currentFn.versions.length() - 1].versionNumber {
                return error("Version already exists");
            }
            if fn.versions[0].versionNumber > (currentFn.versions[currentFn.versions.length() - 1].versionNumber + 1) {
                int expectedVersion = (currentFn.versions[currentFn.versions.length() - 1].versionNumber + 1);
                return error("Version number greater than expected. Expected version number: " + expectedVersion.toString());
            }

            //#####   ADD VERSION TO ARRAY IF NO ERRORS OCCUR   #####
            currentFn.versions.push(fn.versions[0]);

            //#####   RETURN SUCCESS   #####
            return {message: "Successfully added new version"};
        }

        //#####   ADD FUNCTION TO MAP IF IT DOESNT EXIST   #####
        functions[fn.details.name] = fn;

        //#####   RETURN SUCCESS   #####
        return {message: "Successfully added new function"};
    }
    //#################################################
    //#################################################
    //
    //
    //
    //
    //
    //#################################################
    //      ADD FNS
    //#################################################
    remote function add_fns(stream<AddFnsRequest, grpc:Error?> clientStream) returns AddFnsResponse|error {

        string[] returnMessages = []; //     STORES LIST OF SUCCESS AND ERROR RESULTS

        //#################################################
        //      LOOP THROUGH EACH REQEUST IN STREAM
        //#################################################
        error? e = clientStream.forEach(function(AddFnsRequest value) {
            Function fn = value.fn;

            //#################################################
            //      CHECK IF MULTIPLE VERSIONS ARE PRESENT
            //#################################################
            if fn.versions.length() > 1 {
                returnMessages.push("Failed to add fn: " + fn.details.name + ". To many versions supplied when only 1 is expected.");
            } else {

                //#################################################
                //      CHECK IF FN ALREADY EXISTS
                //#################################################
                if functions.hasKey(fn.details.name) {
                    //#####   ADD ERROR MESSAGE TO RETURN MSG   #####
                    returnMessages.push("Failed to add fn: " + fn.details.name + ". Fn already exists");
                } else {
                    //#####   ADD TO FUNCTIONS MAP   #####
                    functions[fn.details.name] = fn;
                    returnMessages.push("Successfully added fn: " + fn.details.name);
                }
            }
        });

        //#################################################
        //      CHECK IF ERROR OCCURED IN CLIENTSTREAM
        //#################################################
        if e is error {
            return error("An unexpected error occured");
        }

        //#################################################
        //      RETURN RESULT ARRAY
        //#################################################
        return {messages: returnMessages};
    }
    //#################################################
    //#################################################
    //
    //
    //
    //
    //
    //#################################################
    //      DELETE FN
    //#################################################
    remote function delete_fn(DeleteFnRequest value) returns DeleteFnResponse|error {
        if functions.hasKey(value.fnName) {

            //###########################################################
            //      DELETE ENTIRE FN IF NO VERSION 0 IS ENTERED
            //###########################################################
            if value.versionNumber == 0 {
                any|error err = functions.remove(value.fnName);
                if err is error {
                    return error("Failed to remove function");
                }
                return {message: "Successfully removed function"};

            }

            //###########################################################
            //     DELETE SPECIFIC VERSION OF FN
            //###########################################################
            if value.versionNumber > 0 {

                Function fn = functions.get(value.fnName);
                int i = 0;
                boolean reorder = false;

                if fn.versions.length() > 0 {

                    //###################################
                    //     FIND SPECIFC VERSION
                    //###################################
                    while i < fn.versions.length() {

                        //################################################################################
                        //     IF VERSION IS REMOVED, DECREASE VERSION NUMBERS OF SUBSEQUENT VERSIONS
                        //################################################################################
                        if reorder {
                            fn.versions[i].versionNumber -= 1;
                        }

                        //###################################
                        //     REMOVE VERSION IF FOUND
                        //###################################
                        if fn.versions[i].versionNumber == value.versionNumber {
                            any|error err = fn.versions.remove(i);
                            functions[fn.details.name] = fn;
                            reorder = true;
                            i -= 1;
                        }

                        i += 1;

                    }

                    //####################################################
                    //     RETURN MESSAGE BASED ON SUCCESS OF REMOVAL
                    //####################################################
                    if reorder {
                        return {message: "Successfully removed function version"};
                    } else {
                        return error("Failed to remove function version");
                    }

                }
            } else {
                return error("negative version numbers not supported");
            }
        }
        return error("Function not found");
    }

    //#################################################
    //#################################################
    //
    //
    //
    //
    //
    //#################################################
    //      SHOW FN
    //#################################################
    remote function show_fn(ShowFnRequest value) returns ShowFnResponse|error {
        if functions.hasKey(value.fnName) {
            Function fn = functions.get(value.fnName);
            foreach Version vs in fn.versions {
                if vs.versionNumber == value.versionNumber {
                    return {vs: vs};
                }
            }
            return error("Version not found");
        }
        return error("Function not found");
    }

    //#################################################
    //#################################################
    //
    //
    //
    //
    //
    //#################################################
    //      SHOW ALL FNS
    //#################################################
    remote function show_all_fns(ShowAllFnsRequest value) returns stream<ShowAllFnsResponse, error?>|error {
        ShowAllFnsResponse[] reponseToStream = [];
        if functions.hasKey(value.fnName) {
            Function fn = functions.get(value.fnName);
            foreach Version vs in fn.versions {
                ShowAllFnsResponse fnsResponse = {
                    vs: vs
                };
                reponseToStream.push(fnsResponse);
            }
            return reponseToStream.toStream();
        }
        return error("Function not found");
    }

    //#################################################
    //#################################################
    //
    //
    //
    //
    //
    //#################################################
    //      SHOW ALL FNS WITH CRITERIA
    //#################################################
    remote function show_all_with_criteria(stream<ShowAllWithCriteriaRequest, grpc:Error?> clientStream) returns stream<ShowAllWithCriteriaResponse, error?>|error {

        ShowAllWithCriteriaResponse[] responsesToStream = []; //  STORE RESPONSES TO BE STREAMED BACK

        //#################################################
        //      LOOP THROUGH EACH REQEUST IN STREAM
        //#################################################
        error? e = clientStream.forEach(function(ShowAllWithCriteriaRequest value) {

            ShowAllWithCriteriaResponse response = { //  STORES MATCHING FNS FOR CURRENT REQ
                fns: []
            };

            //######################################################
            //      LOOP THROUGH EACH FN TO FIND MATCHING ONES
            //######################################################
            functions.forEach(function(Function fn) {

                //######################################################
                //      
                //      CHECK IF FN LANGUAGE MATCHES REQ LANGUAGE
                //      IF REQ LANGUAGE IS EMPTY CONTINUE AS IF 
                //      MATCH FOUND
                //      
                //######################################################
                if value.language == "" || value.language == fn.details.language {

                    //######################################################
                    //      CHECK IF KEWORDS MATCH
                    //######################################################
                    foreach string reqKeyword in value.kewords {

                        boolean found = false; // KEEPS TRACK OF WHETHER OR NOT MATCH FOUND

                        foreach string fnKeyword in fn.details.keywords {
                            if reqKeyword == fnKeyword {

                                //######################################################
                                //      CHECK IF VERSIONS ARRAY ISNT EMPTY
                                //######################################################
                                if fn.versions.length() > 0 {

                                    //#####   ONLY LATEST VERSION OF FN NEEDED    #####
                                    Function fnToRet = {
                                        details: fn.details,
                                        versions: [
                                        fn.versions[fn.versions.length() - 1] //  LATEST VERSION IS ALWAYS LAST IN ARRAY
                                    ]
                                    };

                                    //#####   PUSH TO RES ARRAY AND END KEYWORD SEARCH    #####
                                    response.fns.push(fnToRet);
                                }
                                found = true;
                                break;
                            }
                        }

                        if found {
                            break;
                        }
                    }
                }

            });

            //################################################################################
            //      PUSH RESPONSE FOR CURRENT REQ TO ARRAY OF RESPONSES TO BE STREAMED BACK
            //################################################################################
            responsesToStream.push(response);
        });

        //#################################################
        //      CHECK IF ERROR OCCURED IN CLIENTSTREAM
        //#################################################
        if e is error {
            return error("An unexpected error occured");
        }

        //######################################################
        //      STREAM RESPONSES BACK TO CLIENT
        //######################################################
        return responsesToStream.toStream();
    }
//#################################################
//#################################################
}
