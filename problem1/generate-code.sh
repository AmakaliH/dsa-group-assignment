
#   Generate code files
bal openapi -i ./api/api.yaml

#   move files into their respective dirs
mv ./client.bal ./client/client.bal

#   create a temp file for the new server definition
#   The user will copy the newly generated functions
#   from this file into the existing api_service.bal file
mv ./api_service.bal ./server/api_service_temp.bal

#   copy type file and remove it once done
cp ./types.bal ./client/types.bal
cp ./types.bal ./server/types.bal
rm ./types.bal

echo /################################################
echo Done generating files
echo /################################################

