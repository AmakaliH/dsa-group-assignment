import ballerina/http;

# Successful response
public type TestResponse record {
    string message;
};

# Api to create and update learner profiles
#
# + clientEp - Connector http endpoint
public client class Client {
    http:Client clientEp;
    public isolated function init(http:ClientConfiguration clientConfig = {}, string serviceUrl = "http://localhost:9090/studentService") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
    }
    #
    # + return - Successful response
    remote isolated function test() returns TestResponse|error {
        string path = string `/test`;
        TestResponse response = check self.clientEp->get(path, targetType = TestResponse);
        return response;
    }
    #
    # + return - Successfully added new user
    remote isolated function addUser(LearnerProfile payload) returns SuccessResponse|error {
        string path = string `/addUser`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        SuccessResponse response = check self.clientEp->post(path, request, targetType = SuccessResponse);
        return response;
    }
    #
    # + return - Successfully removed User
    remote isolated function remove(RemoveUserRequest payload) returns SuccessResponse|error {
        string path = string `/user/remove`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        SuccessResponse response = check self.clientEp->post(path, request, targetType = SuccessResponse);
        return response;
    }
    #
    # + username - existing user's username
    # + return - Successfully retrieved user using username
    remote isolated function userByusername(string username) returns LearnerProfile|error {
        string path = string `/user/${username}`;
        LearnerProfile response = check self.clientEp->get(path, targetType = LearnerProfile);
        return response;
    }
    #
    # + return - Successfully updated new User
    remote isolated function update(UpdateUserRequest payload) returns SuccessResponse|error {
        string path = string `/user/update`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        SuccessResponse response = check self.clientEp->post(path, request, targetType = SuccessResponse);
        return response;
    }
    #
    # + return - Successfully added new learning material
    remote isolated function add(LearningMaterial payload) returns SuccessResponse|error {
        string path = string `/materials/add`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        SuccessResponse response = check self.clientEp->post(path, request, targetType = SuccessResponse);
        return response;
    }
    #
    # + course - existing materials course name
    # + return - Successfully retrieved user using username
    remote isolated function materialsBycourse(string course) returns LearningMaterial|error {
        string path = string `/materials/${course}`;
        LearningMaterial response = check self.clientEp->get(path, targetType = LearningMaterial);
        return response;
    }
    #
    # + return - Successfully added new learning material objects
    remote isolated function addLearningObject(AddLearningObjectRequest payload) returns SuccessResponse|error {
        string path = string `/materials/objects/add`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        SuccessResponse response = check self.clientEp->post(path, request, targetType = SuccessResponse);
        return response;
    }
    #
    # + username - username of user for which the material is returned
    # + return - Successfully retrieved materials for user
    remote isolated function objectsByusername(string username) returns GetMaterialForUserResponse|error {
        string path = string `/materials/objects/${username}`;
        GetMaterialForUserResponse response = check self.clientEp->get(path, targetType = GetMaterialForUserResponse);
        return response;
    }
}
