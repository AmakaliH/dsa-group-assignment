import ballerina/io;

Client ep = check new ({});

public function main() returns error? {

    //#############################################
    //      TEST TO SEE IF SERVER IS WORKING
    //#############################################
    error|TestResponse testRes = ep->test();
    if testRes is error {
        io:println("an error occured: ", testRes.message());
    } else {
        io:println("returned value: ", testRes?.message);
    }
    //#############################################
    //#############################################
    //
    //
    //
    //
    //#############################################
    //      ADD NEW USER
    //#############################################
    LearnerProfile lp = {
        firstname: "John",
        lastname: "Adriaans",
        username: "John-Ad",
        preferred_formats: ["video", "text"],
        past_subjects: [
        {
            course: "DSA",
            score: "C"
        }, 
            {
            course: "DTA",
            score: "C"
        }
        ]
    };
    SuccessResponse|error addUserRes = ep->addUser(lp);
    if addUserRes is error {
        io:println("An error occured: ", addUserRes.message());
    } else {
        io:println("returned value: ", addUserRes.message);
    }
    //######    TRY TO ADD SAME USER    ###########
    addUserRes = ep->addUser(lp);
    if addUserRes is error {
        io:println("An error occured: ", addUserRes.message());
    } else {
        io:println("returned value: ", addUserRes.message);
    }
    //#############################################
    //#############################################
    //
    //
    //
    //
    //#############################################
    //      UPDATE USER
    //#############################################

    UpdateUserRequest updateUserRequest = {
        user: {
            firstname: "Revaldo",
            lastname: "Gertze",
            username: "John-Ad",
            preferred_formats: ["video", "text"],
            past_subjects: [
        {
                course: "WAD",
                score: "B"
            }, 
            {
                course: "ICE",
                score: "D"
            }
        ]
        }
    };
    SuccessResponse|error updateUserResponse = ep->update(updateUserRequest);
    if updateUserResponse is error {
        io:println("An error occured while updating user: ", updateUserResponse.message());
    } else {
        io:println("Update user returned value: ", updateUserResponse.message);
    }
    //######    TRY TO UPDATE SAME USER    ###########
    updateUserRequest.user.username = "userUpdate";
    updateUserResponse = ep->update(updateUserRequest);
    if updateUserResponse is error {
        io:println("An error occured while updating user: ", updateUserResponse.message());
    } else {
        io:println("Update user returned value: ", updateUserResponse.message);
    }
    //#############################################
    //#############################################
    //
    //
    //
    //
    //####################################################################
    //      GET EXISTING USER BY USERNAME (USE USER GENERATED ABOVE)
    //####################################################################
    LearnerProfile|error getUserByUnameRes = ep->userByusername(lp.username);
    if getUserByUnameRes is error {
        io:println("An error occured: ", getUserByUnameRes.message());
    } else {
        io:println("returned value: ", getUserByUnameRes.toString());
    }
    //######    TEST WITH NON-EXISTENT USER    ###########
    getUserByUnameRes = ep->userByusername("fakeUser123");
    if getUserByUnameRes is error {
        io:println("An error occured: ", getUserByUnameRes.message());
    } else {
        io:println("returned value: ", getUserByUnameRes.toString());
    }
    //#############################################
    //#############################################
    //
    //
    //
    //
    //####################################################################
    //     REMOVE USER
    //####################################################################
    RemoveUserRequest removeReq = {
        username: lp.username
    };
    SuccessResponse|error removeUserResponse = ep->remove(removeReq);
    if removeUserResponse is error {
        io:println("An error occured: ", removeUserResponse.detail());
    } 
    else {
        io:println(removeUserResponse.message);
    }
    //######    TEST WITH NON-EXISTENT USER    ###########
    removeUserResponse = ep->remove(removeReq);
    if removeUserResponse is error {
        io:println("An error occured: ", removeUserResponse.detail());
    } 
    else {
        io:println(removeUserResponse.message);
    }
    //####################################################################
    //####################################################################
    //
    //
    //
    //
    //#############################################
    //      ADD NEW LEARNING MATERIAL
    //#############################################
    string|error fileData = getFileAsString("test1.pdf");
    if fileData is error {
        io:println("error: ", fileData.message());
    } else {
        LearningMaterial lm = {
            course: "DSA612S",
            learning_objects: {
                audio: [
                {
                    name: "intro",
                    description: "introduction to dsa",
                    difficulty: "C",
                    fileData: fileData //####      SAME FILE REUSED FOR TESTING PURPOSES
                }, 
                {
                    name: "t1",
                    description: "t1 dsa",
                    difficulty: "B",
                    fileData: fileData //####      SAME FILE REUSED FOR TESTING PURPOSES
                }
                ],
                text: [
                {
                    name: "intro",
                    description: "introduction to dsa",
                    difficulty: "C",
                    fileData: fileData
                }, 
                {
                    name: "t1",
                    description: "t1 dsa",
                    difficulty: "B",
                    fileData: fileData //####      SAME FILE REUSED FOR TESTING PURPOSES
                }
                ],
                video: [
                {
                    name: "intro",
                    description: "introduction to dsa",
                    difficulty: "C",
                    fileData: fileData
                }
                ]
            }
        };

        SuccessResponse|error addLearningMaterial = ep->add(lm);

        if addLearningMaterial is error {
            io:println("An error occured: ", addLearningMaterial.message());
        } else {
            io:println("Success: ", addLearningMaterial.message);
        }

        //######    TRY TO ADD SAME MATERIAL AGAIN (SHOULD RETURN ERROR)    ###########
        addLearningMaterial = ep->add(lm);

        if addLearningMaterial is error {
            io:println("An error occured: ", addLearningMaterial.message());
        } else {
            io:println("Success: ", addLearningMaterial.message);
        }
    }
    //#############################################
    //#############################################
    //
    //
    //
    //
    //#############################################
    //      GET LEARNING MATERIAL BY COURSE NAME
    //#############################################
    LearningMaterial|error retrievedLearningMaterial = ep->materialsBycourse("DSA612S");
    if retrievedLearningMaterial is error {
        io:println("error: ", retrievedLearningMaterial.message());
    } else {
        io:println("Returned output: ", retrievedLearningMaterial.course);
    }
    //######    TRY TO GET NON EXISTENT MATERIAL (SHOULD RETURN ERROR)    ###########
    retrievedLearningMaterial = ep->materialsBycourse("DDDDDD");
    if retrievedLearningMaterial is error {
        io:println("error with materials by course: ", retrievedLearningMaterial.message());
    } else {
        io:println("materials by course returned output: ", retrievedLearningMaterial.course);
    }
    //#############################################
    //#############################################
    //
    //
    //
    //
    //#############################################
    //      GET LEARNING MATERIAL FOR STUDENT
    //#############################################

    //######    ADD TEST STUDENT    ###########
    lp = {
        firstname: "John",
        lastname: "Adriaans",
        username: "John-Ad",
        preferred_formats: ["video", "text"],
        past_subjects: [
        {
            course: "DSA",
            score: "C"
        }, 
            {
            course: "DTA",
            score: "B"
        }, 
        {
            course: "ICE",
            score: "A"
        }
        ]
    };
    addUserRes = ep->addUser(lp);

    //######    GET MATERIALS    ###########
    GetMaterialForUserResponse|error getMaterialForUserRes = ep->objectsByusername(lp.username);
    if getMaterialForUserRes is error {
        io:println("Error occured while getting materials for user: ", getMaterialForUserRes.message());
    } else {
        //######    SHOULD ONLY RETURN C LEVEL TOPICS    ###########
        io:println("materials for user: ", getMaterialForUserRes.courses[0].topics[0].topic);
    }
    //#############################################
    //#############################################
    //
    //
    //
    //
    //#############################################
    //      ADD LEARNING OBJECT
    //#############################################
    fileData = getFileAsString("test2.pdf");
    if fileData is string {
        AddLearningObjectRequest lmoReq = {
            course: "DSA612S",
            text: [
            {
                name: "intro",
                description: "introduction to dsa",
                difficulty: "easy",
                fileData: fileData
            }
        ],
            audio: [
            {
                name: "intro",
                description: "introduction to dsa",
                difficulty: "easy",
                fileData: fileData
            }
            ],
            video: [
            {
                name: "intro",
                description: "introduction to dsa",
                difficulty: "easy",
                fileData: fileData
            }
            ]
        };

        SuccessResponse|error addLearningObjectRes = ep->addLearningObject(lmoReq);
        if addLearningObjectRes is error {
            io:println("error while adding learning objects: ", addLearningObjectRes.detail());
        } else {
            io:println(addLearningObjectRes.message);
        }
    } else {
        io:println("file not opened");
    }
//#############################################
//#############################################
}

//##################################################
//      READ FILE AND RETURN CONTENTS AS STRING
//##################################################
function getFileAsString(string fileName) returns string|error {
    string path = "../test_files/" + fileName;

    byte[]|io:Error bytes = io:fileReadBytes(path);

    if bytes is io:Error {
        return error("failed to read file");
    } else {
        string|error fileStr = string:fromBytes(bytes);
        if fileStr is error {
            return error("failed to convert bytes to string");
        }
        return fileStr;
    }
}
