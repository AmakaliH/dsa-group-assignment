public type RemoveUserRequest record {
    string username;
};

public type SuccessResponse record {
    string message;
};

public type AddLearningObjectRequest record {
    string course;
    Text[] text?;
    Audio[] audio?;
    Video[] video?;
};

public type LearnerProfile record {
    string username;
    string lastname;
    string firstname;
    string[] preferred_formats;
    record  { string course; string score;} [] past_subjects;
};

public type Video record {
    string name;
    string description;
    string difficulty;
    string fileData;
};

public type Text record {
    string name;
    string description;
    string difficulty;
    string fileData;
};

public type GetMaterialForUserResponse record {
    Course[] courses;
};

public type Audio record {
    string name;
    string description;
    string difficulty;
    string fileData;
};

public type Course record {
    string course;
    Topic[] topics;
};

public type Topic record {
    string topic;
    Text text;
    Audio audio;
    Video video;
};

public type LearningMaterial record {
    string course;
    record  { Audio[] audio; Video[] video; Text[] text;}  learning_objects;
};

public type UpdateUserRequest record {
    LearnerProfile user;
};

public type ErrorResponse record {
    string message;
};
